﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Bobbing : MonoBehaviour
{
    // Set starting Y posiion for later call back
    float originalY;

    // set how much position changes and how fast
    public float floatStrength = 1;

    // Start is called before the first frame update
    void Start()
    {
        // making the original y additive
        this.originalY = this.transform.position.y;
    }

    // Update is called once per frame
    void Update()
    {
        // actually does the  moving
        transform.position = new Vector3(transform.position.x, originalY + ((float)Math.Sin(Time.time) * floatStrength), transform.position.z);
    }
}

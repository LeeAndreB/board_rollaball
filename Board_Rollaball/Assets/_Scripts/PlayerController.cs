﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    private Rigidbody rb;
    private int count;
    private int health;

    public Text countText;
    public Text winText;
    public Text healthText;
    public float speed;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        count = 0;
        health = 5;
        SetCountText();
        SetHealthText();
        winText.text = "";
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0f, moveVertical);

        rb.AddForce(movement * speed);
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pick Up"))
        {
            other.gameObject.SetActive(false);
            count = count + 1;
            SetCountText();
        }
    }
    void OnCollisionEnter(Collision other)
        {
            if (other.gameObject.CompareTag("Death Spike"))
            {
                // subtract 1 from health & begin health function
                print("Ouch!");
                health -= 1;
                SetHealthText();
            }
        }

    void SetCountText()
    {
        // updates display
        countText.text = "Count: " + count.ToString();
        // initiates win condition
        if (count >= 24)
        {
            winText.text = "You Win!";
        }
    }
    void SetHealthText()
    {
        //change health text to reflect health variable
        healthText.text = "Health: " + health.ToString();
        //Trigger lose state
        if (health  <= 0)
        {
            //set text to indicate lose state
            winText.text = "Game Over!";
        }
    }

}